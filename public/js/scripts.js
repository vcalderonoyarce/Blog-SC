$(document).ready(function(){


  if($('#post-thumbnail').length) {
    $.adaptiveBackground.run({
      parent: '#null',
      success: function($img, data) {
        var color = data.color;
        $('#post-thumbnail').css('background','linear-gradient(to bottom, rgba(255,255,255,0) 0%, '+color+' 50%)');
        console.log('Success!', $img, data);
      }
  });
  }

});
