<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title><?php echo $__env->yieldContent('title'); ?> - VC. Blog</title>
    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,500i,600,600i,700,700i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet">


    <!-- Internal -->
    <link href="/css/defaults.css<?php echo e(caching(true)); ?>" rel="stylesheet">
    <link href="/css/styles.css<?php echo e(caching(true)); ?>" rel="stylesheet">


    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/js/bootstrap.min.js" type="text/javascript"></script>
  </head>
  <body>

    <header id="site-header" class="fixed">
      <div class="container">
        <div class="row">
          <div class="col-3">
            <a id="site-name" href="/">VC<span class="accent-color">.</span></a>
          </div>
          <div class="col-9">

          </div>
        </div>
      </div>
    </header>
    <div id="site-content">
      <?php echo $__env->yieldContent('content'); ?>
    </div>
  </body>
</html>
