<?php $__env->startSection('title','Editando '.$article->title); ?>

<?php $__env->startSection('content'); ?>
  <section id="article-editor">
  <div class="container fs18">

    <h1 class="nomargin mbot30 b fs24">Editando artículo "<?php echo e($article->title); ?>"</h1>

    <form class="form-group" action="index.html" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id">
      <div class="mbot30">
        <label>Título</label>
        <input class="form-control" type="text" value="<?php echo e($article->title); ?>" placeholder="Título" name="title">
      </div>
      <div class="mbot30">
        <label for="slug">Slug</label>
        <input class="form-control" type="text" value="<?php echo e($article->slug); ?>" placeholder="Slug" name="slug">
      </div>
      <div class="mbot30">
        <label for="tags">Tags</label>
        <input value="<?php echo e($article->tags); ?>" type="text" class="form-control tag-input" name="tags">
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="mbot30">
            <label for="demo_link">Enlace demo</label>
            <input value="<?php echo e($article->demo_link); ?>" class="form-control" type="text" placeholder="Ingresar URL demo" name="demo_link">
          </div>
        </div>
        <div class="col-md-6">
          <div class="mbot30">
            <label for="download_link">Enlace descarga</label>
            <input value="<?php echo e($article->download_link); ?>" class="form-control" type="text" placeholder="Ingresar URL descarga" name="download_link">
          </div>
        </div>
      </div>

      <?php if(!empty($article->thumbnail)): ?>
        <div class="mbot30 row">
          <div class="col-md-4 ovhidden radius"><img src="https://cdn.dribbble.com/users/483472/screenshots/2312876/stock--design-dribbble-shot_1x.png" class="responsive d-block"></div>
        </div>
      <?php endif; ?>

      <div class="mbot30">
        <label for="thumbnail">Thumbnail</label>
        <input type="file" name="thumbnail">
      </div>
      <div class="mbot30">
        <label for="redaction"></label>
        <textarea class="form-control" name="redaction"><?php echo e($article->redaction); ?></textarea></div>
      <div>
        <input type="submit" value="Editar artículo">
      </div>
    </form>
    </div>
  </section>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.blog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>