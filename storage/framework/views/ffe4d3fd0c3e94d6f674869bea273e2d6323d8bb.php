<?php $__env->startSection('title','Página principal'); ?>

<?php $__env->startSection('content'); ?>
  <div class="container">
    <div class="row">
      <section id="main-wrapper" class="col-12 col-sm-12 col-md-12 col-lg-9">

          <?php if(empty($articles)): ?>
            <h1 class="nomargin fs36">No se encontraron artículos</h1>
          <?php else: ?>
            <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div class="index-post fs18 mbot30">
                <div class="post-category mbot10">
                  <span>Sin categoría</span>
                </div>
                <div class="post-title-holder">
                  <h1 class="nomargin mont post-title b"><a href="article/<?php echo e($article->slug); ?>" class="incolor"><?php echo e($article->title); ?></a></h1>
                </div>
                <div class="post-meta mbot10">
                  Por <a rel="author" href="https://victorcalderon.cl" class="accentc">Víctor Calderón Oyarce</a>,
                  el <?php echo e($article->created_at->format('d \d\e F \d\e Y')); ?>

                </div>
                <div class="post-description">
                  <?php echo e($article->description); ?>

                </div>

              </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <?php endif; ?>




      </section>
      <aside id="sidebar-wrapper" class="d-block col-12 col-sm-12 col-md-12 col-lg-3">
        SIDEBAR

      </aside>

    </div>

  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.blog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>