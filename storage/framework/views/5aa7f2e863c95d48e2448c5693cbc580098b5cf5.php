<?php $__env->startSection('title',$article->title); ?>

<?php $__env->startSection('content'); ?>
  <div class="container">
    <div id="post-content">

      <h1 id="post-title" class="mont nomargin text-center"><?php echo e($article->title); ?></h1>

      <?php if(!empty($article->tags)): ?>
      <div id="post-tags" class="mbot30 text-center">
        <?php $__currentLoopData = explode(',',$article->tags); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <span class="post-tag inline-block radius"><?php echo e($tag); ?></span>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
      <?php endif; ?>

      <div id="post-body" class="limitwidth mbot30 separator mtop30">
        <?php echo $article->redaction; ?>

        <div class="clearfix"></div>
      </div>
    </div>

  </div>

  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.blog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>