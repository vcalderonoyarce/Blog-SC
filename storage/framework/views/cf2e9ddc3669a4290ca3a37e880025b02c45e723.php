<?php $__env->startSection('title',$article->title); ?>

<?php $__env->startSection('content'); ?>
  <div class="container">
    <div id="post-content">

      <h1 id="post-title" class="mont nomargin text-center"><?php echo e($article->title); ?></h1>

      <?php if(!empty($article->tags)): ?>
      <div id="post-tags" class="mbot30 text-center">
        <?php $__currentLoopData = explode(',',$article->tags); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <span class="post-tag inline-block radius"><?php echo e($tag); ?></span>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
      <?php endif; ?>

      <div class="text-center fs16 mbot30 mtop20">

          Escrito por <a href="https://victorcalderon.cl/">Víctor Calderón Oyarce</a>, el <?php echo e($article->created_at->format('d \d\e F \d\e Y')); ?>

      </div>

      <div class="text-center mtop30 mbot30" id="post-sharer">
          <a target="blank" data-title="Facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e(url('/')); ?>/article/<?php echo e($article->slug); ?>" class="inline-block share-icon fb"><i class="fab fa-facebook fs36 valign"></i></a>
          <a target="blank" data-title="Twitter" href="https://twitter.com/share?text=<?php echo e($article->description); ?>&amp;url=<?php echo e(url('/')); ?>/article/<?php echo e($article->slug); ?>&amp;hashtags=<?php echo e($article->tags); ?>" class="tw inline-block share-icon"><i class="fab fa-twitter fs36 valign"></i></a>
          <a target="blank" data-title="Pinterest" href="https://pinterest.com/pin/create/button/?url=<?php echo e(url('/')); ?>/article/<?php echo e($article->slug); ?>&amp;media=https://blog.victorcalderon.cl/uploads/b-custom-selects.png&amp;description=<?php echo e($article->title); ?> - <?php echo e($article->description); ?>" class="inline-block share-icon pin"><i class="fab fa-pinterest fs36 valign"></i></a>
          <a target="blank" data-title="Google+" href="https://plus.google.com/share?url=<?php echo e(url('/')); ?>/article/<?php echo e($article->slug); ?>" class="inline-block share-icon gplus"><i class="fab fa-google-plus-g fs36 valign"></i></a>
          <a target="blank" data-title="LinkedIn" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo e(url('/')); ?>/article/<?php echo e($article->slug); ?>&amp;title=<?php echo e($article->title); ?>&amp;summary=<?php echo e($article->description); ?>&amp;source=" class="inline-block share-icon lin"><i class="fab fa-linkedin fs36 valign"></i></a>
          <a target="blank" data-title="Copiar URL" href="<?php echo e(url('/')); ?>/article/<?php echo e($article->slug); ?>" class="inline-block share-icon clip copy-to-clipboard" data-content="<?php echo e(url('/')); ?>/article/<?php echo e($article->slug); ?>"><i class="fas fa-paperclip fs36 valign"></i></a>
      </div>

      <div id="post-body" class="limitwidth mbot30 separator mtop30">
        <?php echo $article->redaction; ?>

        <div class="clearfix"></div>
      </div>
    </div>

  </div>

  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.blog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>