<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />


    <title>@yield('title') - VC. Blog</title>


        <!-- Scripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{ url('/')}}/vendors/jquery.adaptive-backgrounds/jquery.adaptive-backgrounds.js"></script>
        <script src="{{ url('/') }}/js/scripts.js{{ caching(true) }}"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,500i,600,600i,700,700i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i|Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet">


    <!-- Internal -->
    <link href="{{ url('/') }}/css/defaults.css{{ caching(true) }}" rel="stylesheet">
    <link href="{{ url('/') }}/css/styles.css{{ caching(true) }}" rel="stylesheet">
    <link href="{{ url('/') }}/css/responsive.css{{ caching(true) }}" rel="stylesheet">


  </head>
  <body>
    <header id="site-header" class="fixed">
      <div class="container">
        <div class="row">
          <div class="col-3">
            <a id="site-name" class="incolor fs24 bolder" href="/">VC<span class="accentc">.</span></a>
          </div>
          <div class="col-9">

          </div>
        </div>
      </div>
    </header>
    <div id="site-content">
      @yield('content')
    </div>
  </body>
</html>
