@extends('layouts.blog')
@section('title',$article->title)

@section('content')
  <div class="container">
  <div id="post">

    <div class="row">
      <div class="col-12 col-lg-9">


          <div id="post-thumbnail" class="text-center relative ovhidden whitefont" data-colored="1">
            @if(!empty($article->thumbnail))
              <img class="blurry-post-img d-block absolute" src="{{ url('/') }}/images/{{$article->thumbnail}}" alt="{{$article->title}}">
              <img id="post-img" data-adaptive-background="1" class="fwidth d-block relative" src="{{ url('/') }}/images/{{$article->thumbnail}}" alt="{{$article->title}}">
              @endif
              <div id="post-meta" class="relative ">
                <div id="post-date" class="mbot20">{{$article->created_at->format('d F Y')}}</div>
                <h1 id="post-title" class="block relative nomargin">{{$article->title}}</h1>
                <div id="post-description" class="fs18">{{$article->description}}</div>
              </div>
              <div id="post-info" class="relative">

                  <div id="author-data" class="upp inline-block">
                    <img src="{{ url('/') }}/img/vcalderonoyarce.jpg" alt="Víctor Calderón Oyarce, vcalderonoyarce, @vcalderonoyarce" id="small-avatar">
                    &nbsp;&nbsp;Por <a class="whitefont" href="https://victorcalderon.cl/">Víctor Calderón Oyarce</a></div>

                <div id="post-sharer" class="inline-block">
                    <a target="blank" data-title="Facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ url('/') }}/article/{{$article->slug}}" class="inline-block share-icon fb"><i class="fab fa-facebook fs24 valign"></i></a>
                    <a target="blank" data-title="Twitter" href="https://twitter.com/share?text={{$article->description}}&amp;url={{ url('/') }}/article/{{$article->slug}}&amp;hashtags={{$article->tags}}" class="tw inline-block share-icon"><i class="fab fa-twitter fs24 valign"></i></a>
                    <a target="blank" data-title="Pinterest" href="https://pinterest.com/pin/create/button/?url={{ url('/') }}/article/{{$article->slug}}&amp;media=https://blog.victorcalderon.cl/uploads/b-custom-selects.png&amp;description={{$article->title}} - {{$article->description}}" class="inline-block share-icon pin"><i class="fab fa-pinterest fs24 valign"></i></a>
                    <a target="blank" data-title="Google+" href="https://plus.google.com/share?url={{ url('/') }}/article/{{$article->slug}}" class="inline-block share-icon gplus"><i class="fab fa-google-plus-g fs24 valign"></i></a>
                    <a target="blank" data-title="LinkedIn" href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{ url('/') }}/article/{{$article->slug}}&amp;title={{$article->title}}&amp;summary={{$article->description}}&amp;source=" class="inline-block share-icon lin"><i class="fab fa-linkedin fs24 valign"></i></a>
                    <a target="blank" data-title="Copiar URL" href="{{ url('/') }}/article/{{$article->slug}}" class="inline-block share-icon clip copy-to-clipboard" data-content="{{ url('/') }}/article/{{$article->slug}}"><i class="fas fa-paperclip fs24 valign"></i></a>
                </div>
              </div>

          </div>





          <div id="post-content" class="whitebg">
            @if(!empty($article->tags))
            <div id="post-tags" class="mbot30 text-center">
              @foreach(explode(',',$article->tags) as $tag)
                <span class="post-tag inline-block radius">{{$tag}}</span>
              @endforeach
            </div>
            @endif

            <div id="post-body" class="mbot30 separator mtop30">
              {!!$article->redaction!!}
              <div class="clearfix"></div>
            </div>
          </div>

      </div>

    </div>


  </div>
  </div>

@endsection
