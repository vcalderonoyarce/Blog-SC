@extends('layouts.blog')
@section('title','Página principal')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-9">
        <section id="main-wrapper">
          @if (empty($articles))
            <h1 class="nomargin fs36">No se encontraron artículos</h1>
          @else
            @foreach ($articles as $article)
              <div class="index-post fs18 mbot30">
                <div class="post-category mbot10">
                  <span>Sin categoría</span>
                </div>
                <div class="post-title-holder">
                  <h1 class="nomargin mont post-title b"><a href="article/{{$article->slug}}" class="incolor">{{$article->title}}</a></h1>
                </div>
                <div class="post-meta mbot10">
                  Por <a rel="author" href="https://victorcalderon.cl" class="accentc">Víctor Calderón Oyarce</a>,
                  el
                </div>
                <div class="post-description">
                  {{$article->description}}
                </div>

              </div>
            @endforeach

            {{$articles->links()}}

          @endif
      </section>
      </div>
      <aside id="sidebar-wrapper" class="d-block col-12 col-sm-12 col-md-12 col-lg-3">
        SIDEBAR

      </aside>

    </div>

  </div>
@endsection
