<?php

// Helper personalizado con funciones específicas

if(!function_exists('endsWith')) {
  function endsWith($string,$find){
      return substr($string, -strlen($find)) == $find ? true : false;
  }
}

if(!function_exists('fileExt')) {
  function fileExt($file_name) {
	   return substr(strrchr($file_name,'.'),1);
  }
}


if(!function_exists('removeChars')) {
  function removeChars($string,$array = []) {
    $r = $string;
    foreach($array as $k => $item) {
      $r = str_replace($item,'',$r);
    }
    return $r;
  }
}

if(!function_exists('random_string')){
  function randomString($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));
    for ($i= 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }
    return $key;
  }
}

  if(!function_exists('boolval')) { // Función para convertir una variable en boolean (Nativa de PHP, no soportada en PHP 5.4 e inferiores)
    function boolval($var){
      return !! $var;
    }
  }


  if(!function_exists('array_to_xml')) {
    function array_to_xml( $data, &$xml_data) {
      foreach($data as $key => $value ) {
        if(is_numeric($key) ){
          $key = 'item-'.$key; // Correxión de los problemas de arrays numéricos...
        }
        if(is_array($value) ) {
          $subnode = $xml_data->addChild($key);
          array_to_xml($value, $subnode); // Se repite infinito en arrays dentro de arrays...
        } else {
          $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
       }
    }
  }

  if(!function_exists('generarXML')) {
    function generarXML($data = [],$root = null) { // Genera un XML a partir de información dada en un ARRAY...
      $root = ($root != null) ? $root : '<?xml version="1.0"?><data></data>'; // Marcado inicial del XML (Default)
      $xml_data = new SimpleXMLElement($root);
      array_to_xml($data,$xml_data);
      return $xml_data->asXML();
    }
  }


  if(!function_exists('miles')) {
    function miles($numero) { // Formatea números a formato de miles (Con separador de punto)
      $numero = number_format(intval($numero),0,",",".");
      return $numero;
    }
  }

  if(!function_exists('caching')) {
    function caching($dev = true) {
      return ($dev == true) ? '?v='.date("YmdHis") : null;
    }
  }
  if(!function_exists('bytes')) {
      function bytes($bytes, $force_unit = NULL, $format = NULL, $si = TRUE){ // Tranforma bytes a su equivalente unidad con sufijo...
          $format = ($format === NULL) ? '%01.2f %s' : (string) $format;
          if ($si == FALSE OR strpos($force_unit, 'i') !== FALSE) {
              $units = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
              $mod   = 1024;
          }
          else {
              $units = array('B', 'kB', 'MB', 'GB', 'TB', 'PB');
              $mod   = 1000;
          }
          if (($power = array_search((string) $force_unit, $units)) === FALSE){
              $power = ($bytes > 0) ? floor(log($bytes, $mod)) : 0;
          }
          return sprintf($format, $bytes / pow($mod, $power), $units[$power]);
      }
  }
